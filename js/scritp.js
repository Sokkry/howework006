
let prices = document.getElementById("get_total_money");
let store = document.getElementById("store");
let t_start = document.getElementById("t_start");
let e_end = document.getElementById("e_end");
let get_total_time = document.getElementById("get_total_time");

let btn_clear = document.getElementById("btn_clear");
let btn_start = document.getElementById("btn_start");
let btn_stop = document.getElementById("btn_stop");

// =========== start button start
btn_start.onclick = ()=>{  
    let time = new Date();
    let time_start = time.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })
    t_start.innerHTML = time_start
        let time_start_store = time
        let total_h_start = Math.floor(time_start_store / 60000)
        store.innerHTML = total_h_start

        btn_start.style.display = 'none'
        btn_stop.style.display = 'block'
}
// =========== end button start

// =========== start button stopt
btn_stop.onclick = ()=>{
    let dates = new Date()
    var time_end = dates.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })
    e_end.innerHTML = time_end

    let get_start_t = store.innerText;
    let time_end_store = dates
    let total_h_end = Math.floor(time_end_store / 60000)
    let total_time = total_h_end - get_start_t
    get_total_time.innerHTML = total_time

    if(total_time <= 15){
        prices.innerHTML = "500"
    }else if(total_time > 15 && total_time <= 30){
        prices.innerHTML = "1000"
    }else if(total_time > 30 && total_time <= 60){
        prices.innerHTML = "1500"
    }else if(total_time > 60){
        prices.innerHTML = "2000"
    }
    

    btn_stop.style.display = 'none'
    btn_clear.style.display = 'block'
}
// =========== end button stop

//========== start button clear 
btn_clear.onclick = ()=>{
    prices.innerHTML = "0"
    store.innerHTML = ""
    t_start.innerHTML = "00:00"
    e_end.innerHTML = "00:00"
    get_total_time.innerHTML = "0"
    btn_clear.style.display = 'none'
    btn_start.style.display = 'block'
}
//========== end button clear 

// ======== start real time
var timing = setInterval(() => {
    var date = new Date()
    document.getElementById("real_date").innerHTML = date.toDateString()
    document.getElementById("real_time").innerHTML = date.toLocaleTimeString();
}, 1000);
// ========= end real time